# Android Been文件批量重命名脚本

#### 介绍
批量的Been文件重命名脚本工具，可以将工程中使用Been类名替换成另一个名字，支持Java/Kotlin。

#### 使用说明

1.  下载BeenRename.gradle到app目录下
2.  引入脚本
```
//在app下的build.gradle中引入脚本
apply from: "BeenRename.gradle"
```
3.  加入编译链（如果你仅想手动执行Task的话可忽略）

```
afterEvaluate {
    android.applicationVariants.all { variant ->
        // 只给 release 注册
        if (variant.name.endsWith('Release')){
            //println ">>>>>> variant.name $variant.name"
            tasks.eachWithIndex { task, index ->
                //println ">>>>>> task.name $variant.name $task.name"
                if (task.name == "pre${variant.name.capitalize()}Build") {
                    println ">>>>>>>>>>>> configure $variant.name $task.name dependsOn renameBeenProcesser"
                    task.configure {
                        dependsOn 'renameBeenProcesser'
                    }
                }
            }
        }
    }
}
```
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request